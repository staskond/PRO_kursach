﻿namespace ShadesOfGray
{
    public enum FilterType
    {
        BlackWhite = 0,
        Negative = 1,
        Sepia = 2,
        Binary = 3
    }
}
