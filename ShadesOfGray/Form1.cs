﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ShadesOfGray
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region Fields

        private List<Bitmap> bitmap = new List<Bitmap>();
        private List<Bitmap> resultBitmap = new List<Bitmap>();
        private bool isMultyLoading;

        #endregion

        #region Methods

        private void Filtrate(FilterType filterType)
        {
            if (pictureBox1.Image == null && !isMultyLoading) // если изображение в pictureBox1 не существует - возвращаем управление
                return;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            if (!isMultyLoading)
            {
                var filter = ImageFiltrator.GetImage(filterType, new Bitmap(pictureBox1.Image));
                if (filter != null)
                    pictureBox2.Image = filter;
                stopWatch.Stop();
                MessageBox.Show("Обработано одно изображение за " + stopWatch.Elapsed.TotalSeconds + " секунд", "Обработано",
                            MessageBoxButtons.OK, MessageBoxIcon.None);
                return;
            }
            resultBitmap.Clear();
            resultBitmap.AddRange(ImageFiltrator.GetMultyImage(filterType, bitmap));
            stopWatch.Stop();
            MessageBox.Show("Обработано " +resultBitmap.Count.ToString()+ " изображений за " + stopWatch.Elapsed.TotalSeconds + " секунд", "Обраотано",
                        MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        #endregion

        #region OpenImageButtons
        // кнопка Открыть
        private void OpenButton_Click(object sender, EventArgs e)
        {

            // диалог для выбора файла
            var ofd = new OpenFileDialog
            {
                // фильтр форматов файлов
                Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"
            };
            // если в диалоге была нажата кнопка ОК
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                isMultyLoading = false;
                bitmap.Clear();
                try
                {
                    // загружаем изображение
                    pictureBox1.Image = new Bitmap(ofd.FileName);
                }
                catch // в случае ошибки выводим MessageBox
                {
                    MessageBox.Show("Невозможно открыть выбранный файл", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void bMultyOpen_Click(object sender, EventArgs e)
        {
            // диалог для выбора файла
            var ofd = new OpenFileDialog
            {
                Multiselect = true,
                // фильтр форматов файлов
                Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"
            };
            // если в диалоге была нажата кнопка ОК
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                isMultyLoading = true;
                bitmap.Clear();
                try
                {
                    // загружаем изображения
                    for (var i = 0; i < ofd.FileNames.Length; i++)
                    {
                        bitmap.Add(new Bitmap(ofd.FileNames[i]));
                    }
                }
                catch // в случае ошибки выводим MessageBox
                {
                    MessageBox.Show("Невозможно открыть выбранный файл", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region SaveImagesButton

        // кнопка Сохранить
        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (pictureBox2.Image != null && !isMultyLoading) // если изображение в pictureBox2 имеется
            {
                var sfd = new SaveFileDialog
                {
                    Title = "Сохранить картинку как...",
                    OverwritePrompt = true, // показывать ли "Перезаписать файл" если пользователь указывает имя файла, который уже существует
                    CheckPathExists = true, // отображает ли диалоговое окно предупреждение, если пользователь указывает путь, который не существует
                                            // фильтр форматов файлов
                    Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*",
                    ShowHelp = true // отображается ли кнопка Справка в диалоговом окне
                };
                // если в диалоге была нажата кнопка ОК
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        // сохраняем изображение
                        pictureBox2.Image.Save(sfd.FileName);
                    }
                    catch // в случае ошибки выводим MessageBox
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                return;
            }
            var FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var count = 0;
                    foreach (var bitMap in resultBitmap)
                    {
                        count++;
                        if (@FBD.SelectedPath.EndsWith(@"\\"))
                        {
                            bitMap.Save(@FBD.SelectedPath + count + ".JPG");
                            continue;
                        }
                        bitMap.Save(FBD.SelectedPath + @"\" + count + ".JPG");
                    }
                    // сохраняем изображение
                    MessageBox.Show("Изображения сохранены по адресу " + FBD.SelectedPath, "Успешно",
                        MessageBoxButtons.OK, MessageBoxIcon.None);
                }
                catch // в случае ошибки выводим MessageBox
                {
                    MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        #endregion

        #region FilterButtons

        // кнопка Ч/Б
        private void GrayButton_Click(object sender, EventArgs e)
        {
            Filtrate(FilterType.BlackWhite);
        }

        private void buttonNegative_Click(object sender, EventArgs e)
        {
            Filtrate(FilterType.Negative);
        }


        private void bSepia_Click(object sender, EventArgs e)
        {
            Filtrate(FilterType.Sepia);
        }

        private void bBinary_Click(object sender, EventArgs e)
        {
            Filtrate(FilterType.Binary);
        }

        #endregion

    }
}
