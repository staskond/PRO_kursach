﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace ShadesOfGray
{
    public static class ImageFiltrator
    {
        static object locker = new object();
        public static Bitmap GetImage(FilterType filterType, Bitmap bitmap)
        {
            if (bitmap == null) // если изображение в pictureBox1 не существует - возвращаем управление
                return null;

            return GetProcessedBitmap(filterType, bitmap);
        }

        public static List<Bitmap> GetMultyImage(FilterType filterType, List<Bitmap> bitmaps)
        {
            if (bitmaps == null) // если изображение в pictureBox1 не существует - возвращаем управление
                return null;

            var result = new List<Bitmap>();
            Parallel.ForEach(bitmaps, (currentFile) =>
            {
                var bmp = GetProcessedBitmap(filterType, currentFile);

                lock (locker)
                {
                    result.Add(bmp);
                }
            });
            return result;
        }

        private static Bitmap GetProcessedBitmap(FilterType filterType, Bitmap bitmap)
        {
            // создаём Bitmap из изображения, находящегося в pictureBox1
            var bmp = new Bitmap(bitmap);
            var pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Получаем адрес первой линии.
            var ptr = bmpData.Scan0;

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            var numBytes = bmpData.Stride * bmp.Height;
            var widthBytes = bmpData.Stride;
            var rgbValues = new byte[numBytes];

            // Копируем значения в массив.
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            Parallel.For(0, rgbValues.Length, counter =>
            {//Так как шаг у нас 3, то мі проверяем чтобы счетчик делился на 3
                if (counter == 0 || counter % 3 == 0)
                {
                    if (counter + 2 > rgbValues.Length || counter + 1 > rgbValues.Length || counter + 3 > rgbValues.Length)
                        return;

                    ImageHandler(ref rgbValues, counter, filterType);
                }
            });
            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, numBytes);

            // Разблокируем набор данных изображения в памяти.
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        private static void ImageHandler(ref byte[] rgbValues, int count, FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.BlackWhite:
                    var BlackWhiteRgb = rgbValues[count] + rgbValues[count + 1] + rgbValues[count + 2];

                    var color_BlackWhite = Convert.ToByte(BlackWhiteRgb / 3);

                    rgbValues[count] = color_BlackWhite;
                    rgbValues[count + 1] = color_BlackWhite;
                    rgbValues[count + 2] = color_BlackWhite;
                    break;
                case FilterType.Negative:
                    rgbValues[count] = Convert.ToByte(255 - rgbValues[count]);
                    rgbValues[count + 1] = Convert.ToByte(255 - rgbValues[count + 1]);
                    rgbValues[count + 2] = Convert.ToByte(255 - rgbValues[count + 2]);
                    break;
                case FilterType.Sepia:

                    var SepiaRgb = rgbValues[count] + rgbValues[count + 1] + rgbValues[count + 2];

                    var color_Sepia = (double)(SepiaRgb / 3);
                    rgbValues[count] = Convert.ToByte(color_Sepia);
                    rgbValues[count + 1] = Convert.ToByte(color_Sepia * 0.95);
                    rgbValues[count + 2] = Convert.ToByte(color_Sepia * 0.82);
                    break;
                case FilterType.Binary:
                    var SepiaRgb1 = rgbValues[count] + rgbValues[count + 1] + rgbValues[count + 2];

                    var color_Sepia1 = (double)(SepiaRgb1 / 3);
                    if (color_Sepia1 < 127)
                    {
                        rgbValues[count] = Convert.ToByte(0);
                        rgbValues[count + 1] = Convert.ToByte(0);
                        rgbValues[count + 2] = Convert.ToByte(0);
                        break;
                    }
                    rgbValues[count] = Convert.ToByte(255);
                    rgbValues[count + 1] = Convert.ToByte(255);
                    rgbValues[count + 2] = Convert.ToByte(255);
                    break;
            }
        }
    }
}